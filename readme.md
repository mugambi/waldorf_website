# SERVER SETUP

## SIMPLE GIT SETUP
```
python3 -m venv .venv
source .venv/bin/activate
pip install -r requirements.txt



```

### GET DATABASE
```
scp sammy@nairobiwaldorfschool.ac.ke:/home/sammy/waldorf_website/db.sqlite3 .
```

### PUSH DATABASE
```
scp db.sqlite3 sammy@nairobiwaldorfschool.ac.ke:/home/sammy/waldorf_website/
```


## USER PROFILE
```
# useradd sammy
# passwd sammy
# usermod -aG sudo sammy              #add user to sudo group ... for fedora use wheel as the group
# usermod -a -G www-data  sammy       #add user to group www-data
# su - sammy                          #test sammy
# mkdir sammy
# chsh -s /usr/bin/bash


## LOGIN FROM LOCAL COMPUTER
`$ ssh sammy@138.68.55.251`
--------------------------------------------
```
#### GENERATE SSH KEYS FOR AUTO-LOGIN

`$ ssh-keygen -t rsa`

copy and paste the public key in local computer whose name is id_rsa.pub.


#### SSH Remote Computer
```
$ ssh-keygen -t rsa
$ chmod 755 ~/.ssh
$ chmod 644 ~/.ssh/authorized_keys
##copy and paste the public key in laptop found in id_rsa.pub. to generate it use: ssh-keygen -t rsa
```


### PERMSIONS
```
$ sudo mkdir /home/sammy
$ sudo chown -R sammy:www-data /home/sammy
$ sudo chgrp -R www-data /home/sammy
$ chmod -R 775 /home/sammy
$ chmod -R g+s /home/sammy
```

#### Install pip3
```
# apt update
# apt ugrade
# apt install python3-pip
# apt install virtualenv
# apt install gunicorn
`$ sudo pip3 install virtualenv`
`$ virtualenv --python=python3 venv`

```


####  Create DIR
`$ git clone https://mugambi@bitbucket.org/mugambi/waldorf_website.git`

`touch /tmp/waldrof.sock`

`$ cd ~/waldorf_backend`

#### TO UPDATE GIT FROM REMOTE BITBUCKET
`$ git pull origin master`

#### TO PUSH TO SERVER GIT FROM REMOTE BITBUCKET
`$ git push -u origin master`


#### OPEN VIRTUAL VENV
```
$ bash
$ virtualenv venv
$ source venv/bin/activate
```
`deactivate`          #To get off the virtual environment



### INSTALL PYTHON LIBRARIES
`$ (vnenv)$ pip3 install django djangorestframework sorl-thumbnail sorl-thumbnail-serializer-field django-ckeditor pillow gunicorn`

### CREATE DATABASE AND MIGRATE
```
$ (vnenv)$ python3 manage.py migrate
$ (vnenv)$ python3 manage.py makemigrations
$ (vnenv)$ python3 manage.py migrate
$ (vnenv)$ python3 manage.py createsuperuser
$ (vnenv)$ python3 manage.py collectstatic
$ sudo cp -R ~/waldorf_backend/static/* /var/www/nairobiwaldorfschool.ac.ke/static/
```



##### FIREWALL **** IGNORE *****
~~# sudo ufw allow 8000~~

~~# sudo iptables -I INPUT -p tcp --dport 8000 -j ACCEPT~~

~~$ (vnenv)$ python3 manage.py runserver ### TEST the server~~



#### GUNICORN SETUP
`$ (vnenv)$ gunicorn --bind 0.0.0.0:8000 waldorf.wsgi:application` #TEST GUNICORN



### GUNICORN SYSTEMD SERVICE
`sudo nano /etc/systemd/system/gunicorn.service`

```
[Unit]
Description=gunicorn daemon
After=network.target

[Service]
User=sammy
Group=www-data
WorkingDirectory=/home/sammy/waldorf_website
ExecStart=/home/sammy/waldorf_website/venv/bin/gunicorn --workers 3 --bind unix:/tmp/waldorf.sock waldorf.wsgi:application

[Install]
WantedBy=multi-user.target
```


`$ sudo systemctl start gunicorn` #Start Gunicorn

`$ sudo systemctl enable gunicorn` #Enable on Startup

`$ sudo systemctl status gunicorn` #TEST gunicorn. ensure the output has "Active: active (running)"




### NGIX CONFIGURE
`$ sudo nano /etc/nginx/sites-available/waldorf` #CREATE nginx conf file and copy below

```
# Default server configuration
#
server {
        client_max_body_size 30M;

        listen 80 default_server;
        listen [::]:80 default_server;

        root /home/sammy/waldorf_website;

        # Add index.php to the list if you are using PHP
        index index.html index.htm index.nginx-debian.html;

        server_name _;

        location / {
                proxy_pass http://unix:/tmp/waldorf.sock;
        }

        location /static {
                root /home/sammy/waldorf_website;
        }

        location /media {
                root /home/sammy/waldorf_website;
        }
}
```

`$ sudo rm /etc/nginx/sites-enabled/default `  #Remove the Defualt settings

`$ sudo ln -s /etc/nginx/sites-available/waldorf /etc/nginx/sites-enabled`     #ENABLE the configurations

`$ sudo nginx -t` #TEST the configurations

`$ sudo systemctl restart nginx`

`change sendfile on; to sendfile off in the nginx.conf file`

### GIT
`git config --global user.email "mugambi.kimathi@gmail.com"`

`git config --global user.name "mugambi"`

`git add -A`

`git commit -m "syncronize"`

`git push origin master`

`git pull orgin master`


### VUEJS
`npm run build`

`scp -r dist/ sammy@138.68.55.251:/var/www/nairobiwaldorfschool.ac.ke/`   #copy the distribution to the server

`scp -r static/waldorf sammy@138.68.55.251:/var/www/nairobiwaldorfschool.ac.ke/static/`
