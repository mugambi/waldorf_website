# Create your views here.
from django.http import HttpResponse
from django.template import loader
from .forms import AppointmentForm, NewsletterForm
from .models import *
from django.http import HttpResponseRedirect
from random import randint
import datetime


def home(request):
  template = loader.get_template('home.html')

  pts = ParentTestimonials.objects.all()
  pts_random_index = randint(0,ParentTestimonials.objects.count()-1)

  context = {
    'form':AppointmentForm(),
    'quote': pts[pts_random_index],
    'news_form':NewsletterForm(),
    'events': Event.objects.all().order_by('order_no').filter(active=True),
    'title': "Home",
    'backgroundImage': '/static/img/home_header_bg'+str(randint(0, 3))+'.jpg',
    'color': 'rgb(7, 153, 72)',
    'colorTranslucent': 'rgba(7, 153, 72,0.8)'
  }
  return HttpResponse(template.render(context, request))


def aboutUs(request):
  template = loader.get_template('aboutus.html')
  context = {
    'form': AppointmentForm(),
    'title': "About Us",
    'news_form': NewsletterForm(),
    'backgroundImage': '/static/img/aboutus_header_bg.jpg',
    'color': 'rgb(95, 148, 142)',
    'colorTranslucent': 'rgba(95, 148, 142,0.8)'
  }
  return HttpResponse(template.render(context, request))


def environment(request):
  template = loader.get_template('environment.html')
  context = {
    'title': "Environment",
    'news_form': NewsletterForm(),
    'backgroundImage': '/static/img/aboutus_header_bg.jpg',
    'color': 'rgb(155,186,31)',
    'colorTranslucent': 'rgba(155,186,31,0.8)'
  }
  return HttpResponse(template.render(context, request))


def team(request):
  template = loader.get_template('team.html')

  context = {
    'title': "Our Team",
    'karenTeam':TeamMembers.objects.all().filter(type='KAREN').order_by('order_no'),
    'lavingtonTeam': TeamMembers.objects.all().filter(type='LAVINGTON').order_by('order_no'),
    'administrationTeam': TeamMembers.objects.all().filter(type='ADMINISTRATION').order_by('order_no'),
    'news_form': NewsletterForm(),
    'backgroundImage': '/static/img/aboutus_header_bg.jpg',
    'color': 'rgb(95, 148, 142)',
    'colorTranslucent': 'rgba(95, 148, 142,0.8)'
  }
  return HttpResponse(template.render(context, request))

def governance(request):
  template = loader.get_template('governance.html')
  context = {
    'title': "Governance",
    'news_form': NewsletterForm(),
    'backgroundImage': '/static/img/aboutus_header_bg.jpg',
    'color': 'rgb(95, 148, 142)',
    'colorTranslucent': 'rgba(95, 148, 142,0.8)'
  }
  return HttpResponse(template.render(context, request))


def contacts(request):
  template = loader.get_template('contacts.html')
  context = {
    'title': " Contact Us",
    'form': AppointmentForm(),
    'news_form': NewsletterForm(),
    'backgroundImage': '/static/img/gallery_header_bg.jpg',
    'color': 'rgb(64, 130, 152)',
    'colorTranslucent': 'rgba(64, 130, 152,0.8)'
  }
  return HttpResponse(template.render(context, request))


def blog(request):
  template = loader.get_template('blog.html')
  context = {
    'title': "Blog",
    'news_form': NewsletterForm(),
    'backgroundImage': '/static/img/OrangeHeaders.jpg',
    'color': 'rgb(230, 81, 0)',
    'colorTranslucent': 'rgba(230, 81, 0,0.8)'
  }
  return HttpResponse(template.render(context, request))


def events(request):
  template = loader.get_template('events.html')
  context = {
    'title': "Calendar & Events",
    'events':Event.objects.all().order_by('order_no'),
    'news_form': NewsletterForm(),
    'backgroundImage': '/static/img/OrangeHeaders.jpg',
    'color': 'rgb(7, 153, 72)',
    'colorTranslucent': 'rgba(7, 153, 72,0.8)'
  }
  return HttpResponse(template.render(context, request))


def gallery(request):
  template = loader.get_template('gallery.html')
  context = {
    'title': "Gallery",
    'gallery': Gallery.objects.all().order_by('order_no'),
    'categories': GalleryCategory.objects.all(),

    'news_form': NewsletterForm(),
    'backgroundImage': '/static/img/header3.jpg',
    'color': 'rgb(64, 130, 152)',
    'colorTranslucent': 'rgba(64, 130, 152,0.8)'
  }
  return HttpResponse(template.render(context, request))


def admission(request):
  template = loader.get_template('admission.html')
  context = {
    'title': "Admission & Policies",
    'downloadsProspective':Download.objects.all().filter(prospective=True).order_by('order_no'),
    'downloadsCurrent': Download.objects.all().filter(prospective=False).order_by('order_no'),
    'news_form': NewsletterForm(),
    'backgroundImage': '/static/img/Purple_Headers.jpg',
    'color': 'rgb(42, 76, 127)',
    'colorTranslucent': 'rgba(42, 76, 127, 0.8)'
  }
  return HttpResponse(template.render(context, request))


def primary_curriculum(request):
  template = loader.get_template('primary_curriculum.html')
  context = {
    'form': AppointmentForm(),
    'title': "Primary",
    'news_form': NewsletterForm(),
    'classes': PrimaryCurriculum.objects.all().order_by('order_no'),
    'backgroundImage': '/static/img/curriculum_header_bg.jpg',
    'color': 'rgb(234, 157, 63)',
    'colorTranslucent': 'rgba(234, 157, 63,0.8)'
  }
  return HttpResponse(template.render(context, request))

def high_school_curriculum(request):
  template = loader.get_template('high_school_curriculum.html')
  context = {
    'form': AppointmentForm(),
    'title': "High School",
    'news_form': NewsletterForm(),
    'classes': HighSchoolCurriculum.objects.all().order_by('order_no'),
    'backgroundImage': '/static/img/curriculum_header_bg.jpg',
    'color': 'rgb(234, 157, 63)',
    'colorTranslucent': 'rgba(234, 157, 63,0.8)'
  }
  return HttpResponse(template.render(context, request))


def kindergarten_curriculum(request):
  template = loader.get_template('kindergarten_curriculum.html')
  context = {
    'form': AppointmentForm(),
    'classes': KindergartenCurriculum.objects.all().order_by('order_no'),
    'title': "Kindergarten",
    'news_form': NewsletterForm(),
    'backgroundImage': '/static/img/curriculum_header_bg.jpg',
    'color': 'rgb(234, 157, 63)',
    'colorTranslucent': 'rgba(234, 157, 63,0.8)'
  }
  return HttpResponse(template.render(context, request))


def education(request):
  template = loader.get_template('education.html')
  context = {
    'form': AppointmentForm(),
    'title': "Waldorf Curriculum",
    'news_form': NewsletterForm(),
    'backgroundImage': '/static/img/curriculum_header_bg.jpg',
    'color': 'rgb(234, 157, 63)',
    'colorTranslucent': 'rgba(234, 157, 63,0.8)'
  }
  return HttpResponse(template.render(context, request))


def history(request):
  template = loader.get_template('history.html')
  context = {
    'title': "History",
    'news_form': NewsletterForm(),
    'backgroundImage': '/static/img/aboutus_header_bg.jpg',
    'color': 'rgb(155,186,31)',
    'colorTranslucent': 'rgba(155,186,31,0.8)'
  }
  return HttpResponse(template.render(context, request))


def thankyou(request):
  template = loader.get_template('thankyou.html')
  context = {
    'title': "",
    'news_form': NewsletterForm(),
    'backgroundImage': '/static/img/gallery_header_bg.jpg',
    'color': 'rgb(235, 119, 76)',
    'colorTranslucent': 'rgba(235, 119, 76,0.8)',
    'message':'Thank you, we shall get back to you'
  }
  return HttpResponse(template.render(context, request))

def appointmentForm(request):
  if request.method == 'POST':
    form = AppointmentForm(request.POST)
    if form.is_valid():
      first_name = form.cleaned_data['first_name']
      last_name = form.cleaned_data['last_name']
      mobile = form.cleaned_data['mobile']
      email = form.cleaned_data['email']
      school = form.cleaned_data['school']
      message = form.cleaned_data['message']

      # Save the Detials
      ad = AppointmentDetails(first_name=first_name, last_name=last_name, mobile=mobile, email=email,school=school,  date=datetime.datetime.now(), message=message)
      ad.save()



      template = loader.get_template('thankyou.html')
      context = {
        'title': "Thank You",
        'news_form': NewsletterForm(),
        'backgroundImage': '/static/img/gallery_header_bg.jpg',
        'color': 'rgb(235, 119, 76)',
        'colorTranslucent': 'rgba(235, 119, 76,0.8)',
        'message':'Thank you for your request, we shall get back to you'
      }
      return HttpResponse(template.render(context, request))
    else:
      return HttpResponseRedirect('/home')
  else:
    return HttpResponseRedirect('/home')

def registerNewsletter(request):
  if request.method == 'POST':
    form = NewsletterForm(request.POST)
    if form.is_valid():
      email = form.cleaned_data['email']
      ne = NewsletterEmails(email=form.cleaned_data['email'])
      ne.save()
      template = loader.get_template('thankyou.html')
      context = {
        'title': "Newsletter",
        'news_form': NewsletterForm(),
        'backgroundImage': '/static/img/gallery_header_bg.jpg',
        'color': 'rgb(235, 119, 76)',
        'colorTranslucent': 'rgba(235, 119, 76,0.8)',
        'message': 'Thank you for registering for our Newsletter'
      }
      return HttpResponse(template.render(context, request))

def contactForm(request):
  if request.method == 'POST':
    form = AppointmentForm(request.POST)
    if form.is_valid():
      first_name = form.cleaned_data['first_name']
      last_name = form.cleaned_data['last_name']
      mobile = form.cleaned_data['mobile']
      email = form.cleaned_data['email']
      message = form.cleaned_data['message']


      # Save the Detials
      cd = ContactDetails(first_name=first_name, last_name=last_name, mobile=mobile, email=email, message=message)
      cd.save()

      template = loader.get_template('thankyou.html')
      context = {
        'title': " Contact Us",
        'news_form': NewsletterForm(),
        'backgroundImage': '/static/img/gallery_header_bg.jpg',
        'color': 'rgb(234, 157, 63)',
        'colorTranslucent': 'rgba(234, 157, 63,0.8)',
        'message': 'Thank you, we shall get back to you'
      }
      return HttpResponse(template.render(context, request))


def jobs(request):
  template = loader.get_template('job.html')
  context = {
    'title': "Jobs",
    'news_form': NewsletterForm(),
    'jobs': Job.objects.all().filter(active=True).order_by("order_no"),
    'backgroundImage': '/static/img/aboutus_header_bg.jpg',
    'color': 'rgb(155,186,31)',
    'colorTranslucent': 'rgba(155,186,31,0.8)'
  }
  return HttpResponse(template.render(context, request))


def tenders(request):
  template = loader.get_template('tender.html')
  context = {
    'title': "Tenders",
    'news_form': NewsletterForm(),
    'tenders': Tender.objects.all().order_by("order_no"),
    'backgroundImage': '/static/img/aboutus_header_bg.jpg',
    'color': 'rgb(155,186,31)',
    'colorTranslucent': 'rgba(155,186,31,0.8)'
  }
  return HttpResponse(template.render(context, request))

from django.db.models import Q
def newsletter(request):
  template = loader.get_template('newsletter.html')
  context = {
    'title': "Media & Documents",
    'documents':NewsletterDocuments.objects.all().order_by('-date'),
    'categoryNewsletters': NewsletterDocuments.objects.all().order_by('-date').filter(category="NEWSLETTER"),
    'categoryDocuments': NewsletterDocuments.objects.all().order_by('-date').filter(category="DOCUMENT"),
    'categoryMultimedia': NewsletterDocuments.objects.all().order_by('-date').filter(category="MULTIMEDIA"),
    'news_form': NewsletterForm(),
    'backgroundImage': '/static/img/aboutus_header_bg.jpg',
    'color': 'rgb(155,186,31)',
    'colorTranslucent': 'rgba(155,186,31,0.8)'
  }
  return HttpResponse(template.render(context, request))
