from django.contrib import admin
from django.template.loader import render_to_string
from sorl.thumbnail.admin import AdminImageMixin

# for dealing with flatpages
from django.contrib.flatpages.admin import FlatPageAdmin as FlatPageAdminOld
from django.contrib.flatpages.admin import FlatpageForm as FlatpageFormOld
from django import forms
from ckeditor.widgets import CKEditorWidget
from django.contrib.flatpages.models import FlatPage
from django.forms import widgets
# Register your models here.
from .models import *


# class BlogAdmin(AdminImageMixin, admin.ModelAdmin):
#   list_display = ('date', 'topic', 'thumb')
#   list_display_links = ('date', 'topic')
#
#   def thumb(self, obj):
#     return render_to_string('thumb.html', {
#       'image': obj.image
#     })
#
#   thumb.allow_tags = True
#
#
# admin.site.register(Blog, BlogAdmin)


class EventAdmin(AdminImageMixin, admin.ModelAdmin):
  list_display = ('order_no','topic','active', 'start_Date', 'end_Date', 'start_Time', 'end_Time', 'location', 'thumb')
  list_display_links = ('topic', 'thumb', 'location')


  def thumb(self, obj):
    return render_to_string('thumb.html', {
      'image': obj.image
    })

  thumb.allow_tags = True


admin.site.register(Event, EventAdmin)

class GallerryCategoryAdmin(admin.ModelAdmin):
  list_display = ('name',)
  list_display_links = ('name',)

admin.site.register(GalleryCategory, GallerryCategoryAdmin)


class GalleryAdmin(AdminImageMixin, admin.ModelAdmin):
  list_display = ('thumb', 'order_no', 'category', 'timestamp', 'description',)
  list_display_links = ('thumb','order_no', 'description',)

  def thumb(self, obj):
    return render_to_string('thumb.html', {
      'image': obj.image
    })

  thumb.allow_tags = True


admin.site.register(Gallery, GalleryAdmin)


class NewsletterAdmin(admin.ModelAdmin):
  list_display = ('email', 'timestamp')


admin.site.register(NewsletterEmails, NewsletterAdmin)


class ContactAdmin(admin.ModelAdmin):
  list_display = ('first_name', 'last_name', 'mobile', 'email', 'message', 'timestamp')


admin.site.register(ContactDetails, ContactAdmin)


class AppointmentAdmin(admin.ModelAdmin):
  list_display = ('first_name', 'last_name', 'date', 'email', 'message','school', 'timestamp')


admin.site.register(AppointmentDetails, AppointmentAdmin)


class ParentTestimonialsAdmin(admin.ModelAdmin):
  list_display = ('parent_quote', 'parent_name')


admin.site.register(ParentTestimonials, ParentTestimonialsAdmin)


class TeamMembersAdmin(AdminImageMixin, admin.ModelAdmin):
  list_display = ('first_name', 'last_name', 'designation', 'type', 'thumb')
  list_display_links = ('first_name', 'last_name','designation')
  def thumb(self, obj):
    return render_to_string('thumb.html', {
      'image': obj.image
    })

  thumb.allow_tags = True


admin.site.register(TeamMembers, TeamMembersAdmin)


class KindergartenCurriculumAdmin(AdminImageMixin, admin.ModelAdmin):
  list_display = ('order_no','topic', 'image', 'thumb')
  list_display_links = ('order_no', 'topic')
  def thumb(self, obj):
    return render_to_string('thumb.html', {
      'image': obj.image
    })

  thumb.allow_tags = True
admin.site.register(KindergartenCurriculum, KindergartenCurriculumAdmin)


class PrimaryCurriculumAdmin(AdminImageMixin, admin.ModelAdmin):
  list_display = ('order_no','topic', 'image', 'thumb')
  list_display_links = ('order_no', 'topic')
  def thumb(self, obj):
    return render_to_string('thumb.html', {
      'image': obj.image
    })

  thumb.allow_tags = True
admin.site.register(PrimaryCurriculum, PrimaryCurriculumAdmin)


class HighSchoolCurriculumAdmin(AdminImageMixin, admin.ModelAdmin):
  list_display = ('order_no','topic', 'image', 'thumb')
  list_display_links = ('order_no', 'topic')
  def thumb(self, obj):
    return render_to_string('thumb.html', {
      'image': obj.image
    })

  thumb.allow_tags = True
admin.site.register(HighSchoolCurriculum, HighSchoolCurriculumAdmin)


class DownloadAdmin(admin.ModelAdmin):
  list_display = ('order_no','display_name','file',)
admin.site.register(Download,DownloadAdmin)


#for flat pages
class FlatpageForm(FlatpageFormOld):
  content = forms.CharField(widget=CKEditorWidget())

  class Meta:
    # this is not automatically inherited from FlatpageFormOld
    model = FlatPage
    fields = ['id', 'title', 'content']


class FlatPageAdmin(FlatPageAdminOld):
  form = FlatpageForm


admin.site.unregister(FlatPage)
admin.site.register(FlatPage, FlatPageAdmin)


class JobAdmin(admin.ModelAdmin):
  list_display = ('order_no', 'title','timestamp','active')
  list_display_links = ('order_no', 'title', 'timestamp')

admin.site.register(Job, JobAdmin)


class TenderAdmin(admin.ModelAdmin):
  list_display = ('order_no', 'title', 'timestamp')
  list_display_links = ('order_no', 'title', 'timestamp')


admin.site.register(Tender, TenderAdmin)

class NewsletterDocumentsAdmin(admin.ModelAdmin):
  list_display = ('date','category','type','title','file')
  list_display_links = ('date','category','type', 'title')


admin.site.register(NewsletterDocuments, NewsletterDocumentsAdmin)
