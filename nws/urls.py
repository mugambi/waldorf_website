
from nws import views
from django.urls import path
from django.conf import settings
from django.conf.urls.static import static


urlpatterns = [
    path('',views.home, name='home'),
    path('home',views.home, name='home'),
    path('aboutUs',views.aboutUs, name='aboutUs'),
    path('environment',views.environment, name='environment'),
    path('team',views.team, name='team'),
    path('history',views.history, name='history'),
    path('governance',views.governance, name='governance'),
    path('kindergarten_curriculum',views.kindergarten_curriculum, name='kindergarten_curriculum'),
    path('primary_curriculum',views.primary_curriculum, name='primary_curriculum'),
    path('high_school_curriculum',views.high_school_curriculum, name='high_school_curriculum'),
    path('education',views.education, name='education'),
    path('admission',views.admission, name='admission'),
    path('gallery',views.gallery, name='gallery'),
    path('events',views.events, name='events'),
    path('blog',views.blog, name='blog'),
    path('contacts',views.contacts, name='contacts'),
    path('appointment',views.appointmentForm, name='appointment'),
    path('contactus',views.contactForm, name='contact_us_form'),
    path('newsletter',views.registerNewsletter, name='newsletter'),
    path('jobs', views.jobs, name='jobs'),
    path('tenders', views.tenders, name='tenders'),
    path('thankyou', views.thankyou, name='thankyou'),
    path('news-letter', views.newsletter, name='newsletter')

]+ static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)