from nws.models import Blog, Gallery, Event, NewsletterEmails, ContactDetails, AppointmentDetails, TeamMembers
from rest_framework import routers, serializers, viewsets
from sorl_thumbnail_serializer.fields import HyperlinkedSorlImageField
DATE_INPUT_FORMATS = ['%Y-%m-%d']
DATE_FORMAT ='%Y-%m-%d'

class BlogSerializer (serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Blog
        fields = ('url', 'id', 'topic', 'date', 'image', 'timestamp', 'info', 'thumbnail')
    # A thumnail image, sorl options and read-only
    thumbnail = HyperlinkedSorlImageField(
        '400x300',
        options={"crop": "top"},
        source='image',
        read_only=True
    )



    # A larger version of the image, allows writing
    image = HyperlinkedSorlImageField('1280')

class EventSerializer (serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Event
        fields = ('url', 'id', 'topic', 'start_Date_Time', 'end_Date_Time', 'location', 'image', 'timestamp', 'info')
    # A larger version of the image, allows writing
    image = HyperlinkedSorlImageField('1280', options={"crop": "top"},)



class GallerySerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Gallery
        fields = ('url', 'id', 'image', 'date', 'type', 'description', 'thumbnail')

    # A thumnail image, sorl options and read-only
    thumbnail = HyperlinkedSorlImageField(
        '300x300',
        options={"crop": "top"},
        source='image',
        read_only=True
    )

    # A larger version of the image, allows writing
    image = HyperlinkedSorlImageField('1280', options={"crop": "top"},)


class NewsletterSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = NewsletterEmails
        fields = ('email',)


class ContactsSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = ContactDetails
        fields = ('first_name', 'last_name', 'email', 'mobile', 'message', )


class AppointmentSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = AppointmentDetails
        fields = ('first_name', 'last_name', 'mobile', 'email', 'date', 'message', )

class TeamMembersSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = TeamMembers
        fields = ('first_name','last_name','passport_photo','thumb')

    thumbnail = HyperlinkedSorlImageField(
        '180x180',
        options={"crop": "center"},
        source='passport_photo',
        read_only=True
    )

    image = HyperlinkedSorlImageField('368x230', options={"crop": "center"}, )

class KindergartenCurriculumSerializer (serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Blog
        fields = ('id', 'topic', 'image',  'info', 'thumbnail')
    # A thumnail image, sorl options and read-only
    thumbnail = HyperlinkedSorlImageField(
        '180x180',
        options={"crop": "top"},
        source='image',
        read_only=True
    )


    # A larger version of the image, allows writing
    image = HyperlinkedSorlImageField('368x230', options={"crop": "center"},)