# Create your models here.
from django.contrib.contenttypes.models import ContentType
from sorl.thumbnail import ImageField
from ckeditor.fields import RichTextField
from django.db import models
from django.core.mail import send_mail
import datetime



class Blog(models.Model):
    topic = models.CharField(max_length=300)
    date = models.DateField(default=datetime.date.today)
    image = ImageField(blank=True, null=True, upload_to="images/blog/")
    timestamp = models.DateTimeField(auto_now_add=True)
    info = RichTextField(blank=True, null=True)



class Event(models.Model):
    topic = models.CharField(max_length=300)
    start_Date = models.DateField(blank=True, null=True, help_text="Please use the following format: <em>YYYY-MM-DD</em>.")
    end_Date = models.DateField(blank=True, null=True, help_text="(Optional) Please use the following format: <em>YYYY-MM-DD</em>.")
    start_Time = models.TimeField(blank=True, null=True, help_text="Please use the following 24Hr format: <em>HH:MM:SS</em>.")
    end_Time = models.TimeField(blank=True, null=True, help_text="Please use the following 24Hr format: <em>HH:MM:SS</em>.")
    order_no = models.IntegerField(default=1, unique=True, help_text="Order Number of how you would like the event placement to appear. <br/> e.g. <b>1</b> is the first, <b>2</b> is the second<br/>Note: There should be no duplicate order numbers")
    location = models.CharField(blank=True, null=True, max_length=300)
    image = ImageField(blank=True, null=True, upload_to="images/event/")
    timestamp = models.DateTimeField(auto_now_add=True)
    info = RichTextField(blank=True, null=True)
    active = models.BooleanField(default=False,
                                      help_text="Show on the Home Page and as Default on Event Page")



class GalleryCategory(models.Model):
    name = models.CharField(max_length=300)

    def __unicode__(self):
        return self.name

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "Gallery Category"
        verbose_name_plural = "Gallery Categories"


class Gallery(models.Model):
    image = ImageField(upload_to="images/gallery/")
    timestamp = models.DateTimeField(auto_now_add=True)
    description = models.CharField(max_length=300, null=True, blank=True)
    category = models.ForeignKey(GalleryCategory, on_delete=models.CASCADE)
    order_no = models.IntegerField(default=1, unique=True, help_text="Order Number")


    class Meta:
        verbose_name = "Gallery Image"
        verbose_name_plural = "Gallery Images"





class NewsletterEmails(models.Model):
    email = models.EmailField(max_length=300)
    timestamp = models.DateTimeField(auto_now_add=True)

    def save(self, *args, **kwargs):
        send_mail(
            ('[WEBSITE] Add to Newsletter: %s' % self.email),
            ('Kindly add the following address to the newsletter.\n\r%s' % self.email),
            'no-reply@nairobiwaldorf.ac.ke',
            ['info@nairobiwaldorf.ac.ke'],
            # ['mugambi.kimathi@gmail.com'],
            fail_silently=False,
        )
        super(NewsletterEmails, self).save(*args, **kwargs)

    class Meta:
        verbose_name = "Newsletter Emails"
        verbose_name_plural = "Newsletter Emails"

class ParentTestimonials(models.Model):
    parent_name = models.CharField(max_length=100, null=True, blank=True)
    parent_quote = models.TextField(null=True, blank=True)

    class Meta:
        verbose_name = "Parents Comments"
        verbose_name_plural = "Parents Comments"
MEDIA_TYPE = (
    ('JPEG', 'Image JPG'),
    ('MP4', 'Video MP4'),
    ('PDF', 'Pdf'),
    ('OTHER', 'Other'),
)

MEDIA_CATEGORY = (
('NEWSLETTER', 'Newsletter'),
('DOCUMENT', 'Document'),
('MULTIMEDIA', 'Multimedia'),
)
# alter table nws_newsletterdocuments
# 	add category text;

class NewsletterDocuments(models.Model):
    file = models.FileField(upload_to="files/")
    category = models.CharField(max_length=10, choices=MEDIA_CATEGORY, default='NEWSLETTER', )
    type = models.CharField(max_length=10, choices=MEDIA_TYPE, default='PDF', )

    title = models.CharField(max_length=256,null=True, blank=True, help_text="Title of the Media")
    date = models.DateField(null=True, blank=True,
                            help_text="Uploaded Newsletters are ordered by date: latest to newest")
    description = RichTextField(null=True, blank=True)
    timestamp = models.DateTimeField(auto_now_add=True)

    class Meta:
        verbose_name = "Media Document"
        verbose_name_plural = "Media Documents"

class ContactDetails(models.Model):
    first_name = models.CharField(max_length=30, null=True, blank=True)
    last_name = models.CharField(max_length=30, null=True, blank=True)
    mobile = models.CharField(max_length=20, null=True, blank=True)
    email = models.EmailField(max_length=100, null=True, blank=True)
    message = models.TextField(null=True, blank=True)
    timestamp = models.DateTimeField(auto_now_add=True)

    def save(self, *args, **kwargs):

        message = '''
        Kindly contact the following person:
        First Name: %s
        Last Name: %s
        Mobile: %s
        Email: %s
        Message: %s
        ''' % (self.first_name, self.last_name, self.mobile, self.email, self.message)

        send_mail(
            ('[WEBSITE] Contact from Website: %s %s' % (self.first_name, self.last_name)),
            message,
            'no-reply@nairobiwaldorf.ac.ke',
            ['info@nairobiwaldorf.ac.ke'],
            # ['mugambi.kimathi@gmail.com'],
            fail_silently=False,
        )
        super(ContactDetails, self).save(*args, **kwargs)

    class Meta:
        verbose_name = "Contact Details"
        verbose_name_plural = "Contact Details"


class AppointmentDetails(models.Model):
    first_name = models.CharField(max_length=30, null=True, blank=True)
    last_name = models.CharField(max_length=30, null=True, blank=True)
    date = models.DateField(null=True, blank=True)
    mobile = models.CharField(max_length=20, null=True, blank=True)
    email = models.EmailField(max_length=100,null=True, blank=True)
    school = models.CharField(max_length=100, default="Karen Campus (Miotoni Road)")
    message = models.TextField(null=True, blank=True)
    timestamp = models.DateTimeField(auto_now_add=True)

    def save(self, *args, **kwargs):
        message = '''
        Kindly make appointment:
        First Name: %s
        Last Name: %s
        Mobile: %s
        Email: %s
        School: %s
        Message: %s
      
        ''' % (self.first_name, self.last_name, self.mobile, self.email, self.school, self.message)

        # send_mail(
        #     ('[WEBSITE] Appointment Request: %s %s' % (self.first_name, self.last_name)),
        #     message,
        #     'no-reply@nairobiwaldorf.ac.ke',
        #     ['info@nairobiwaldorf.ac.ke'],
        #     # ['mugambi.kimathi@gmail.com'],
        #     fail_silently=False,
        # )
        super(AppointmentDetails, self).save(*args, **kwargs)

    class Meta:
        verbose_name = "Appointment Emails"
        verbose_name_plural = "Appointment Emails"



TEAM_CHOICES = [
        ('KAREN', "Karen"),
        ('LAVINGTON', "Lavington"),
        ('ADMINISTRATION', "Administration"),
    ]




class TeamMembers(models.Model):
    image = ImageField(blank=True, null=True, upload_to="images/team/")
    first_name = models.CharField(max_length=30, null=True, blank=True)
    last_name = models.CharField(max_length=30, null=True, blank=True)
    designation = models.CharField(max_length=350, null=True, blank=True)
    type = models.CharField(max_length=30, choices=TEAM_CHOICES, default='KAREN', )
    order_no = models.IntegerField(default=1, unique=True, help_text="Order Number")

    class Meta:
        verbose_name = "Team Members"
        verbose_name_plural = "Team Members"


class KindergartenCurriculum(models.Model):
    image = ImageField(blank=True, null=True, upload_to="images/")
    topic = models.CharField(max_length=256, null=True, blank=True)
    order_no = models.IntegerField(default=1, unique=True)
    info = RichTextField(blank=True, null=True)

    class Meta:
        verbose_name = "Kindergarten Curriculum"
        verbose_name_plural = "Kindergarten Curriculums"


class PrimaryCurriculum(models.Model):
    image = ImageField(blank=True, null=True, upload_to="images/")
    topic = models.CharField(max_length=256, null=True, blank=True)
    order_no = models.IntegerField(default=1, unique=True)
    info = RichTextField(blank=True, null=True)

    class Meta:
        verbose_name = "Primary Curriculum"
        verbose_name_plural = "Primary Curriculum"

class  HighSchoolCurriculum(models.Model):
    image = ImageField(blank=True, null=True, upload_to="images/")
    topic = models.CharField(max_length=256, null=True, blank=True)
    order_no = models.IntegerField(default=1, unique=True)
    info = RichTextField(blank=True, null=True)

    class Meta:
        verbose_name = "High School Curriculum"
        verbose_name_plural = "High School Curriculum"


class Download(models.Model):
    file = models.FileField(upload_to="files/")
    display_name = models.CharField(max_length=256)
    timestamp = models.DateTimeField(auto_now_add=True)
    order_no = models.IntegerField(default=1, unique=True, help_text="Order Number of how you would like the download placement to appear.")
    prospective = models.BooleanField(default=True, help_text="checked if parent is prospective, un-checked if parent is current")


class Job(models.Model):
    title = models.CharField(max_length=256)
    description = RichTextField(blank=True, null=True)
    order_no = models.IntegerField(default=1, unique=True)
    active = models.BooleanField(default=False,help_text="Show on the Jobs Page")
    timestamp = models.DateTimeField(auto_now_add=True)


class Tender(models.Model):
    title = models.CharField(max_length=256)
    description = RichTextField(blank=True, null=True)
    order_no = models.IntegerField(default=1, unique=True)
    timestamp = models.DateTimeField(auto_now_add=True)
