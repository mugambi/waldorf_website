from django import forms

SCHOOL_CHOICES=[('Karen Campus (Miotoni Road)','Karen Campus (Miotoni Road)'),
         ('Lavington Campus (Mageta Road)','Lavington Campus (Mageta Road)')]

class AppointmentForm(forms.Form):
  first_name = forms.CharField(max_length=30, label="First Name")
  last_name = forms.CharField(max_length=30, label="Last Name")
  mobile = forms.CharField(max_length=20, label="Mobile")
  email = forms.EmailField(max_length=100, label="Email")
  school = forms.ChoiceField(choices=SCHOOL_CHOICES, label="School Campus")
  message = forms.CharField(widget=forms.Textarea, label="Message")
  

class NewsletterForm(forms.Form):
  email = forms.EmailField(max_length=100, label="Email")


